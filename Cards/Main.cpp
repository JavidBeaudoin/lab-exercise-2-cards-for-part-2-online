// struct for card games

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank // ace high
{
	TWO = 2, THREE =3 , FOUR = 4, FIVE = 5, SIX = 6, SEVEN = 7, EIGHT = 8, NINE = 9, TEN = 10, JACK = 11, QUEEN = 12, KING = 13, ACE = 14
};

enum Suit
{
	SPADES, DIAMONDS, CLUBS, HEARTS
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) {
	
	if (card.rank <= 10) {
		cout << "The " << card.rank;

	}
	else {
		switch (card.rank) {
		case JACK: cout << "The Jack"; break;
		case QUEEN: cout << "The Queen"; break;
		case KING: cout << "The King"; break;
		case ACE: cout << "The Ace"; break;


		}

	}


	switch (card.suit)
	{
		case HEARTS: cout << " of hearts"; break;
		case SPADES: cout << " of spades"; break;
		case CLUBS: cout << " of clubs"; break;
		case DIAMONDS: cout << " of diamonds"; break;


	}

}


Card HighCard(Card card1, Card card2) 
{
	if (card1.rank > card2.rank) 
		return card1;
	else  
		return card2;
	
	
}



int main()
{
	// testing
	
	char input;

	Card card1;
	card1.rank = SIX;
	card1.suit = HEARTS;

	Card card2;
	card2.rank = QUEEN;
	card2.suit = SPADES;

	cout << "The high card is: ";
	PrintCard(HighCard(card1, card2));


	_getch();
	return 0;
}
